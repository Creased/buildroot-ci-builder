#
# Enhanced Buildroot Dockerfile
#

FROM registry.gitlab.com/buildroot.org/buildroot/base:20210922.2200 AS builder

## Environment variable.
ENV DEBIAN_FRONTEND noninteractive

## Switch to root user.
USER root

## Update APT cache.
RUN apt update

## Install dependencies.
RUN apt -fy --no-install-recommends install \
    curl git make g++ gcc-arm-linux-gnueabihf g++-arm-linux-gnueabihf \
    libc6-dev-armhf-cross wget file ca-certificates \
    binutils-arm-linux-gnueabihf cmake && \
    apt clean

## Switch back to br-user.
USER br-user

## Install rustup.
RUN curl https://sh.rustup.rs -sSf | sh -s -- -y

## Rust configuration.
ENV PATH "${HOME}/.cargo/bin/:${PATH}"
ENV RUST_TARGETS "arm-unknown-linux-gnueabihf"
ENV RUST_BACKTRACE 1

## Install cross compilation toolchain.
RUN rustup target add armv7-unknown-linux-gnueabihf && \
    rustup target add armv7-unknown-linux-musleabihf && \
    cargo install cargo-binutils && \
    rustup component add llvm-tools-preview
